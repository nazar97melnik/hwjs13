"use strict";
// 1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
// setTimeout() виконується один раз через певний проміжок часу , а setInterval(`) буде виконуватись раз за разом з тим інтервалом яким ми задамо.
// 2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
// він спрацює лише після всього попереднього коду , тобто спочатку виконується код , а потім сет таймаут з нульовою затримкою
// 3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
// Тому що якщо не прописати clearInterval() то він буде безкінечно виконуватись.

const imgDiv = document.querySelector(".images-wrapper");
const startBtn = document.createElement("button");
startBtn.innerText = "Play";
const pauseBtn = document.createElement("button");
pauseBtn.innerText = "Pause";
imgDiv.insertAdjacentElement("afterend", startBtn);
startBtn.addEventListener("click", function clicked(e) {
  startBtn.removeEventListener("click", clicked);
  startBtn.after(pauseBtn);
  let timerId = setInterval(showImg, 3000);
  function showImg() {
    let activeImg = document.querySelector(".active");
    if (activeImg.nextElementSibling !== null) {
      activeImg.classList.remove("active");
      activeImg.nextElementSibling.classList.add("active");
    } else {
      imgDiv.firstElementChild.classList.add("active");
      activeImg.classList.remove("active");
    }
  }

  pauseBtn.addEventListener("click", function () {
    clearInterval(timerId);
    startBtn.addEventListener("click", clicked);
  });
});
